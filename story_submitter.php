<?php
   session_start();
    require 'database.php';
    $story_title = $_POST["story_title"];
    $story_link = $_POST["story_link"];
    //gets story title and ID
    if(!empty($story_title) && !empty($story_link)){
      $story_stmt = $mysqli->prepare("INSERT INTO stories (story_title, user_id, story_link) VALUES (?,?,?)");
      if(!$story_stmt){
         printf("Query Prep Failed: %s\n", $mysqli->error);
         exit;
      }
      $story_stmt->bind_param('sss', $story_title,$_SESSION["user_id"],$story_link);
      //inserts into story table based on passed in title, the session ID, and passed in link
      $story_stmt->execute();
      $story_stmt->close();
      header('Location: mainpage.php');
    }
    else {
        header('Location: new_story.php');
    }
    
?>