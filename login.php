<!DOCTYPE html>
<!--This form presents the login page, where the user enters their username.-->
<html>
<head>
   <style>
      body{
         background-color: #F6FAFF;
      }
   </style>
<meta charset="utf-8"/>
<title>Login</title>
</head>
<body>
<?php
    session_start();
    if(!empty($_SESSION["errormessage"])){
        echo $_SESSION['errormessage'];
        echo "<br>";
    }
    
    session_destroy();
?>
    <strong style="font-size:15px">Please enter your Username and Password:</strong><br>
    <form action="listing.php" method = "POST">
        <input type ="text" name = "username" id="username"/> <br><!--User enters their username here-->
        <input type ="password" name = "password" id="password"/> <br>
        <input type = "submit" value = "login" name = "login"/> <!--submits username to listing.php that checks if the username matches to users.txt-->
        <input type ="submit" value ="New user" name = "newuser"/>
        <input type= "submit" value= "Log in as a guest" name= "guestuser"/>
    </form>
</body>
</html>
