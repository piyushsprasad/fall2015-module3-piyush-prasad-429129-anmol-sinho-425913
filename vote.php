<!--This form controls the voting of submissions-->
<?php
    require 'database.php';
    session_start();
    $points = $_POST['karmascore'];
    $thisstory = $_POST['storyscore'];
    //updates the story entry with the new karma value
    $score = $mysqli->prepare("UPDATE stories SET karma = ? WHERE story_id = ?");
    if(!$score){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
       }
    if(isset($_POST['upvote'])){//upvotes
        $points = $points + 1;
    }
    elseif(isset($_POST['downvote'])){//downvotes
        $points = $points - 1;
    }
    $score->bind_param('ii',$points,$thisstory);
    $score->execute();
    $score->close();
    header('Location: mainpage.php');
?>