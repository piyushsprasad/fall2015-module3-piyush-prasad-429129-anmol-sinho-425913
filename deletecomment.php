<!--This form controls the deleting of comments-->
<?php
   require 'database.php';
   session_start();
   $commentid = $_POST['commentid'];
   
   if(!empty($commentid)){
      //Deletes the comment using the inputted comment id
      $stmt = $mysqli->prepare("DELETE from comments WHERE comment_id = ?");
      if(!$stmt){
         printf("Query Prep Failed: %s\n", $mysqli->error);
         exit;
      }
      $stmt->bind_param('s', $commentid);
      $stmt->execute();
      $stmt->close();
   }
   //redirects to viewcomments if the story id is inputted
   if(isset($_POST['storyid']))
   {
      $story_id = $_POST['storyid'];
      header('Location: viewcomments.php?storyid='.$story_id.'&Comments=Comments');
   }
   header('Location: userpage.php');
?>