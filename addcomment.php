<!--This form controls the adding of comments-->
<?php
   require 'database.php';
   session_start();
   $user_id = $_SESSION['user_id'];
   $story_id = $_POST['storyid'];
   $thecomment = $_POST['thecomment'];
   if(!empty($story_id) && !empty($thecomment)){
      //Inserts the comment into the comment table
      $stmt = $mysqli->prepare("INSERT INTO comments (story_id, user_id, comment_text) VALUES (?,?,?)");
      if(!$stmt){
         printf("Query Prep Failed: %s\n", $mysqli->error);
         exit;
      }
      $stmt->bind_param('sss',$story_id,$user_id,$thecomment);
      $stmt->execute();
      $stmt->close();
   }
   header('Location: viewcomments.php?storyid='.$story_id.'&Comments=Comments');
?>