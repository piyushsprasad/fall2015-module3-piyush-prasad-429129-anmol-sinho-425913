<!DOCTYPE html>
<html>
<head>
</head>
   <title>View Comments</title>
	<style>
      body{
         background-color: #F6FAFF;
      }
      table, td, th{
         border-collapse: collapse;
			border: 1px solid black;
			padding: 2px;
         text-align: center;
		}
      th {
         height: 30px;
			background-color: #99CCFF;
			color: #ffffff;
		}
      #commentlist td.commenttext{
         text-align: left;
      }
      #commentlist tr.dark td {
			color: #000000;
			background-color: #E0F0FF;
      }
   </style>
   <body>
      <?php
         session_start();
         require 'database.php';
         $user_id = $_SESSION['user_id'];
         $story_id = $_GET['storyid'];
         $stmt = $mysqli->prepare("SELECT story_title,story_link,user_id,users.user_name
                                  FROM stories
                                  JOIN users on (stories.user_id = users.id)
                                  WHERE story_id=?");
         if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
         }
         $stmt->bind_param('s', $story_id);
         $stmt->execute();
         $stmt->bind_result($title, $storylink,$submitted_byID, $submitted_byName);
         $stmt->fetch();
         $stmt->close();
		 //prepares statement
         echo "<strong style='font-size:25px'>".$title."</strong><br>
               Submitted by <strong>".$submitted_byName."</strong>: <a href='".$storylink."'>Link</a><br>";
               
         $stmt2 = $mysqli->prepare("SELECT users.user_name,comment_text,comment_id,user_id
                                  FROM comments
                                  JOIN users on (comments.user_id = users.id)
                                  WHERE story_id=?");
         if(!$stmt2){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
         }
         $stmt2->bind_param('s', $story_id);
         $stmt2->execute();
         $stmt2->bind_result($commenter, $comment, $commentid,$commenterid);
		 //binds comment to commentor and story
         echo "<table border =1 style ='width:50%' id = 'commentlist'>";
         echo "<tr><th>Id</th><th>Username</th><th>Comment</th><th>Delete</th></tr>";
         $islight = TRUE;
         while($stmt2->fetch()){
            if(!$islight){
               echo"<tr class = 'dark'>";
            }
            else{
               echo"<tr>";
            }
            echo "<td>".$commentid."</td>
               <td>".$commenter."</td>
               <td class ='commenttext'>".$comment."</td>";
            if($user_id == $commenterid){
			  //create edit and delete comments buttons which run those actions
               echo"<td>
                  <form action = \"editcommentpage.php\" method =\"POST\">
                  <input type = \"hidden\" name = \"editcomment_id\" value = '".$commentid."'/>
                  <input type =\"hidden\" value ='".$story_id."' name=\"story_id\"/>
                  <input type =\"hidden\" value ='".$commentid."' name=\"commentid\"/>
                  <input type = \"submit\" value = \"Edit\" name = \"Edit\"/> </form>
                  <form action =\"deletecomment.php\" method =\"POST\">
                  <input type =\"submit\" value = \"Delete\" name = \"Delete\">
                  <input type =\"hidden\" value ='".$commentid."' name=\"commentid\"/>
                  <input type =\"hidden\" value ='".$story_id."' name=\"storyid\"/>
                  </form></td>";
            }
            else{
               echo"<td></td>";
            }
            echo"</tr>";
            $islight = !$islight;
         }
         echo "</table><br>";
         $stmt2->close();
         if(!$_SESSION['guest']){
		  //so guests can not add/edit posts
            echo "<form action =\"addcomment.php\" method =\"POST\">
            <input type =\"text\" name = \"thecomment\" id=\"thecomment\" size =\"70\"/><br>
            <input type = \"submit\" value = \"Add a comment\">
            <input type =\"hidden\" value ='".$story_id."' name =\"storyid\"/>
            </form>";
         }
         echo "<form action = \"mainpage.php\" method = \"POST\"> <input type = \"submit\" value = \"Back to Main Page\"> </form>";
      ?>
   </body>
</html>