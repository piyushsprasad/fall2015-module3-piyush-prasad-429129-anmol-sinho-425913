<!--This form presents the mainpage, where the user can view and vote on all the uploaded stories and submit new ones-->
<!DOCTYPE html>
<html>
<head>
		  <title>Main Page</title>
		  <style>
        body{ 
            background-color: #F6FAFF;
        }
		  table, td, th{
				border-collapse: collapse;
				border: 1px solid black;
				padding: 5px;
            text-align: center;
		  }
		  /*table, td, th {
					 
		  }*/
		  th {
            height: 30px;
				background-color: #99CCFF;
				color: #ffffff;
		  }
		  #list tr.dark td {
				color: #000000;
				background-color: #E0F0FF;
		  }
		  #list td.title{
				text-align: left;
		  }
        .p{
        /*padding: 5px;*/
        }
		  </style>
</head>
<body>
		  
		  <?php
				session_start();
					 require 'database.php';
					 $user_id = $_SESSION['user_id'];
					 echo "<strong style='font-size:25px'>Here are the news stories:</strong><br>";
					 echo "<table border =1 style ='width:50%' id = 'list'>";
					 //Sets up the table
					 echo "<tr><th>Score</th><th> Story Title </th> <th> Submitted By: </th> <th> View Link </th> <th> View/Add Comments </th><th>Delete</th></tr>";
					 $stmt = $mysqli->prepare("SELECT karma, story_title, user_id, story_link, story_id, users.user_name
													  FROM stories
													  JOIN users on (stories.user_id = users.id)
													  ORDER by karma DESC");
					 if(!$stmt){ //requests the relevant information of all the stories from the mysql table
								printf("Query Prep Failed: %s\n", $mysqli->error);
								exit;
					 }
					 $stmt->execute();
					 $stmt->bind_result($karma,$title, $submitted_byID, $link, $story_id, $submitted_byName);
					 $islight = TRUE;
					 while($stmt->fetch()){
								if($islight){
									 echo "<tr class='light'>";    
								}
								else{
									 echo "<tr class='dark'>";
								}
								//buttons for the user to vote on the submissision, text for the title and link, and button to view comments
								echo "<form action =\"vote.php\" method =\"POST\"> 
										<td> <input type = \"submit\" name = \"upvote\" value = \"upvote\"/> ".$karma."
										<input type = \"submit\" name = \"downvote\" value = \"downvote\"/>
										<input type = \"hidden\" name = \"karmascore\" value = '".$karma."'/>
										<input type = \"hidden\" name = \"storyscore\" value = '".$story_id."'/></td></form>
										<form action =\"viewcomments.php\" method =\"GET\">
										<td class ='title'>".$title."</td> <td class = 'submittedby'>".$submitted_byName."</td>
										<td class = 'links'><a href='".$link."'>Link</a></td>
										<input type =\"hidden\" value ='".$story_id."' name=\"storyid\"/>
										<td class ='tocomments'><input type = \"submit\" value = \"Comments\" name = \"Comments\"/></td></form>";
								//if the user_id is the same as the submitted user id, allow the user to delete the post
								if($user_id == $submitted_byID){
									echo"<td><form action =\"deletestory.php\" method =\"POST\">
									<input type =\"submit\" value = \"Delete\" name = \"Delete\">
									<input type =\"hidden\" value ='".$story_id."' name=\"story_id\"/>
									<input type =\"hidden\" value=1 name = \"fromwhere\"/>
									</form></td>";
								}
								else{
									echo"<td></td>";
								}
								echo "</tr>";
								$islight = !$islight;
					 }
					 echo "</table><br>";
					 //Displays the logged in user
                echo "<strong>Logged in as ".$_SESSION['user_name']."</strong> ";
					 //If the guest user is not logged in, allow them to submit new stories and view their userpage.
					 if(!$_SESSION['guest']){
						echo "<form action=\"userpage.php\" method =\"GET\"><input type=\"submit\" value =\"View your profile\"></form>";
						echo "<form action = \"new_story.php\" method = \"GET\"> <input type = \"submit\" value = \"Submit a story\"> </form>";
					 }
                echo  "<form action =\"logout.php\" method =\"GET\"><input type = \"submit\" value = \"Log out\"></form>";
					 $stmt->close();

		  ?>
</body>
</html>
















