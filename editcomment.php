<!--This form controls the editing of comments-->
<?php
    session_start();
    require 'database.php';
    $edited_comment = $_POST['thecomment'];
    $editedcomment_id = $_POST['commentid'];
    if (!empty($edited_comment)){
      //updates the comment using the inputted comment and comment id
        $commentedit = $mysqli->prepare("UPDATE comments SET comment_text = ? where comment_id = ?");
        if(!$commentedit){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $commentedit->bind_param('si',$edited_comment,$editedcomment_id);
        $commentedit->execute();
        $commentedit->close();
        header('Location: mainpage.php');
    }
    