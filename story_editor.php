<!--This form controls the editting of stories-->
<?php
    session_start();
    require 'database.php';
    $story_title = $_POST["editstory_title"];
    $story_link = $_POST["editstory_link"];
    $editstory_id = $_POST['edit_id'];
    //brings in story title, link, and id
    if (!empty($story_title) && !empty($story_link)){
        $edit = $mysqli->prepare("UPDATE stories SET story_link = ?,story_title=? where story_id = ?");
        //updates based on story ID.
        if(!$edit){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $edit->bind_param('ssi',$story_link,$story_title,$editstory_id);
        //Sets new values in SQL Database
        $edit->execute();
        $edit->close();
        header('Location: userpage.php');
    }
?>