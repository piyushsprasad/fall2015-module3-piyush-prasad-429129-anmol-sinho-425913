<!DOCTYPE html>
    <html>
        <head>
            <title> Your Page!</title>
			 <style>
        body{
                background-color: #F6FAFF;
        }
		  table, td, th{
					 border-collapse: collapse;
					 border: 1px solid black;
					 padding: 5px;
                text-align: center;
		  }
		  /*table, td, th {
					 
		  }*/
		  th {
                height: 30px;
					 background-color: #99CCFF;
					 color: #ffffff;
		  }
		  #list tr.dark td {
					 color: #000000;
					 background-color: #E0F0FF;
		  }
        #commentlist tr.dark td {
					 color: #000000;
					 background-color: #E0F0FF;
		  }
		  #list td.title{
					 text-align: left;
		  }
        #commentlist td.justifyleft{
					 text-align: left;
		  }
        .p{
        /*padding: 5px;*/
        }
		  </style>
        </head>
        <body>
            <?php
                require 'database.php';
                session_start();
                echo "<strong style='font-size:25px'>Here are your news stories:</strong><br>";
               echo "<table border =1 style ='width:50%' id = 'list'>";
               echo "<tr><th border = 1> Story Title </th><th> View Link </th> <th> View/Add Comments </th><th>Delete</th></tr><br>";
                $user_stmt = $mysqli-> prepare("SELECT story_id,story_link,story_title FROM stories WHERE user_id = ?");
                if(!$user_stmt){
                  	printf("Query Prep Failed: %s\n", $mysqli->error);
                	exit;
                }
               $user_stmt->bind_param('i',$_SESSION['user_id']);
                $user_stmt->execute();
				//selects stories where the submitter is the session user
                $user_stmt->bind_result($mystory_id, $mystory_link,  $mystory_title);
                $islight = TRUE;
                while($user_stmt->fetch()){
                    if($islight){
						echo "<tr class='light'>";    
					}
					else{
                        echo "<tr class='dark'>";
					}
                    $islight = !$islight;
					//Creates table with the stories that user inputted. Can go to comments page, edit, and delete
                    echo "<form action =\"viewcomments.php\" method =\"GET\">
					<td class ='title'>".$mystory_title."</td>
					<td class = 'links'><a href='".$mystory_link."'>Link</a></td>
					<input type = \"hidden\" name = \"storyid\" value = \"$mystory_id\"/>
					<td class ='tocomments'><input type = \"submit\" value = \"Comments\" name = \"Comments\"/></td></form>
					<td><form action = \"editpage.php\" method = \"POST\">
					<input type = \"hidden\" name = \"edit_id\" value = \"$mystory_id\"/>
					<input type = \"submit\" value = \"Edit\" name = \"Edit\"/> </form>
               <form action =\"deletestory.php\" method =\"POST\">
					<input type = \"hidden\" name = \"story_id\" value = \"$mystory_id\"/>
               <input type =\"hidden\" value =\"userpage\" name =\"fromwhere\"/>
               <input type = \"submit\" value = \"Delete\" name = \"Delete\"/>
               </form></td>";
					echo "</tr>";
                }
				echo "</table><br>";
            echo "<strong style='font-size:25px'>Here are your comments:</strong>";
				$user_stmt->close();
            $stmt2 = $mysqli->prepare("SELECT comment_text,comment_id,stories.story_title,stories.story_id
                                  FROM comments
                                  JOIN stories on (stories.story_id = comments.story_id)
                                  WHERE comments.user_id=?");
            if(!$stmt2){
               printf("Query Prep Failed: %s\n", $mysqli->error);
               exit;
            }
            $stmt2->bind_param('s', $_SESSION['user_id']);
            $stmt2->execute();
            $stmt2->bind_result($comment, $commentid,$storytitle,$story_id);
            echo "<table border =1 style ='width:50%' id = 'commentlist'>";
            echo "<tr><th>Story</th><th>Comment</th><th>Delete</th></tr>";
            $islight = TRUE;
            while($stmt2->fetch()){
               if($islight){
						echo "<tr class='light'>";    
					}
					else{
                  echo "<tr class='dark'>";
					}
               echo "<td class = 'justifyleft'>".$storytitle."</td>
                  <td class = 'justifyleft'>".$comment."</td>";
				  //allows for deleting comments
                  echo "<td><form action =\"deletecomment.php\" method =\"POST\">
                     <input type =\"submit\" value = \"Delete\" name = \"Delete\">
                     <input type =\"hidden\" value ='".$commentid."' name=\"commentid\"/>
                     </form></td>";
               echo"</tr><br>";
               $islight = !$islight;
            }
            echo "</table>";
            $stmt2->close();
            echo "<form action =\"mainpage.php\" method = \"POST\"><input type = \"submit\" value = \"Back to Main Page\" name = \"Back to Mainpage\"/></form>";
            echo "<form action =\"logout.php\" method = \"POST\"><input type =\"submit\" value =\"Log out\" name =\"Log out\"/></form>";
            ?>

            
        </body>
    </html>