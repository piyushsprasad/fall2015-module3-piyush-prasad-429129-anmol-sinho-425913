<!--This form presents the listing page, where the users inputted password and username is checked with the database.-->
<!DOCTYPE html>
        
    <html>
        <head>
            <title>Listing</title>
        </head>
    <?php
        session_start();
        $_SESSION['token'] = substr(md5(rand()), 0, 10);
        $username = $_POST['username'];
        $pwd_guess= $_POST['password'];
        require 'database.php';
        if(!empty($_POST["login"])){ 
            $stmt = $mysqli->prepare("select count(*), id, password FROM users WHERE user_name=?");
            
            if(!$stmt){ //checks how many users have the inputted username
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->bind_param('s', $username);
            $stmt->execute();
            $stmt->bind_result($cnt, $user_id, $pwd_hash);
            $stmt->fetch();
            $stmt->close();
            
            echo $cnt;
            if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
               //If one user has the username and the correct password is inputted, log in with the user
                $_SESSION['user_name'] =$username;
                $_SESSION['user_id'] = $user_id;
                $_SESSION['guest'] = False;
                header('Location: mainpage.php');
            }
            else{
               //Else display wrong username/password and return to login page.
                $_SESSION['errormessage'] = "Wrong username/password";
                header('Location: login.php');
            }
        }
        else if(!empty($_POST["newuser"])){
            //If the user clicks new user and wants to create a new account
            $stmt = $mysqli->prepare("SELECT count(*) FROM users WHERE user_name=?");
            if(!$stmt){ //Checks how many users have the inputted username
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->bind_param('s', $username);
            $stmt->execute();
            $stmt->bind_result($cnt);
            $stmt->fetch();
            $stmt->close();
            if($cnt ==0){ //If the username does not exist, create a new account
                $stmt2 = $mysqli->prepare("INSERT INTO users (user_name, password) VALUES (?,?)");
                if(!$stmt2){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt2->bind_param('ss', $username, crypt($pwd_guess));
                $stmt2->execute();
                $stmt2->fetch();
                $stmt2->close();
                
                $stmt3 = $mysqli->prepare("SELECT count(*), id FROM users WHERE user_name=?");
                if(!$stmt3){ //Gets the new user's id
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt3->bind_param('s', $username);
                $stmt3->execute();
                $stmt3->bind_result($cnt, $user_id);
                $stmt3->fetch();
                $stmt3->close();
                
                if( $cnt ==1){ //logs in with the new user and redirects to mainpage.
                    $_SESSION['user_name'] =$username;
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['guest'] = False;
                    header('Location: mainpage.php');
                }
            }
            else{ //Else, the username already exists
                $_SESSION['errormessage'] = "Username already exists.<br>";
                header('Location: login.php');
            }
         }
         else{ //Login as a guest
            $_SESSION['user_name'] ="Guest";
            $_SESSION['user_id'] = -1;
            $_SESSION['guest'] = True;
            header('Location: mainpage.php');
         }
    ?>

</html>
    