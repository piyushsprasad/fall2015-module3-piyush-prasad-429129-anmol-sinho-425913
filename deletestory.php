<!--This form controls the deleting of stories-->
<?php
    require 'database.php';
    session_start();
    $deletestory_id = $_POST['story_id'];
    $fromwhere = $_POST['fromwhere'];
    //Deletes the story using the inputted story id
    $delete = $mysqli->prepare("DELETE FROM stories where story_id = ?");
    if(!$delete){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
    }
    $delete->bind_param('i',$deletestory_id);
    $delete->execute();
    $delete->close();
    header('Location: mainpage.php');
?>